package com.devcamp.bookauthor_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthor_api.model.Author;
import com.devcamp.bookauthor_api.service.AuthorService;

@RestController
public class AuthorController {
    @Autowired
    private AuthorService authorService;

    @CrossOrigin
    @GetMapping("/author-info")
    public Author getAuthorByEmail(@RequestParam(required = true) String email){
        ArrayList<Author> authorlistAuthor = authorService.getListAllAuthor();
        Author result = new Author();
        for (Author author : authorlistAuthor){
            if(author.getEmail().equals(email)){
                result = author;
            }
        }
        return result;
    }

    @CrossOrigin
    @GetMapping("/author-gender")
    public ArrayList<Author> getAuthorByGender(@RequestParam(required = true) char gender){
        ArrayList<Author> authorlistAuthor = authorService.getListAllAuthor();
        ArrayList<Author> result = new ArrayList<>();
        for (Author author : authorlistAuthor){
            if(author.getGender() == gender){
                result.add(author);
            }
        }
        return result;
    }
}
