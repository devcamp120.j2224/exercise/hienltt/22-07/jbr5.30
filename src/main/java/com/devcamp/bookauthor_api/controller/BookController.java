package com.devcamp.bookauthor_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthor_api.model.Book;
import com.devcamp.bookauthor_api.service.BookService;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> getListBook(){
        ArrayList<Book> listBook = bookService.getListBook();
        return listBook;
    }

    @CrossOrigin
    @GetMapping("/book-quantity")
    public ArrayList<Book> getBookByQty(@RequestParam(required = true) int quantityNumber){
        ArrayList<Book> listBook = bookService.getListBook();
        ArrayList<Book> result = new ArrayList<>();
        for(Book book : listBook){
            if(book.getQty() > quantityNumber){
                result.add(book);
            }
        }
        return result;
    }

}
