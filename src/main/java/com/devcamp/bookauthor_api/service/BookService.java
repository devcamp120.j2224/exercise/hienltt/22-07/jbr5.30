package com.devcamp.bookauthor_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.bookauthor_api.model.Book;
@Service
public class BookService {
    AuthorService authorService = new AuthorService();

    Book bk1 = new Book("Naruto", authorService.getListAuthor1(), 150000, 10);
    Book bk2 = new Book("OPM", authorService.getListAuthor2(), 90000, 16);
    Book bk3 = new Book("AOT", authorService.getListAuthor3(), 30000, 100);

    public ArrayList<Book> getListBook(){
        ArrayList<Book> listBook = new ArrayList<>();
        listBook.add(bk1);
        listBook.add(bk2);
        listBook.add(bk3);
        return listBook;
    }
}
