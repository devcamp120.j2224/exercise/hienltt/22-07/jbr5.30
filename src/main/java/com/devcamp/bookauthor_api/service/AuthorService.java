package com.devcamp.bookauthor_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.bookauthor_api.model.Author;
@Service
public class AuthorService {
    Author aut1 = new Author("Ngạn", "ngan@mail", 'm');
    Author aut2 = new Author("Tri", "tri@mail", 'm');
    Author aut3 = new Author("Huyen", "huyen@mail", 'f');
    Author aut4 = new Author("ABC", "abc@mail", 'f');
    Author aut5 = new Author("XYZ", "xyz@mail", 'm');
    Author aut6 = new Author("HUI", "hui@mail", 'f');

    public ArrayList<Author> getListAuthor1(){
        ArrayList<Author> authorlist1 = new ArrayList<>();
        authorlist1.add(aut1);
        authorlist1.add(aut2);
        return authorlist1;
    }

    public ArrayList<Author> getListAuthor2(){
        ArrayList<Author> authorlist2 = new ArrayList<>();
        authorlist2.add(aut3);
        authorlist2.add(aut4);
        return authorlist2;
    }

    public ArrayList<Author> getListAuthor3(){
        ArrayList<Author> authorlist3 = new ArrayList<>();
        authorlist3.add(aut5);
        authorlist3.add(aut6);
        return authorlist3;
    }

    public ArrayList<Author> getListAllAuthor(){
        ArrayList<Author> authorlistAll = new ArrayList<>();
        authorlistAll.addAll(getListAuthor1());
        authorlistAll.addAll(getListAuthor2());
        authorlistAll.addAll(getListAuthor3());
        return authorlistAll;
    }
}
